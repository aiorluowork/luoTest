package com.uplooking.service;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class Clazz {
    
	/*@Autowired
	@Qualifier(value="et")
	private Teacher t;*/
	
	@Resource(name="et")
	private Teacher t;
	
	public void clazz() {
		t.teach();
	}
	
}
