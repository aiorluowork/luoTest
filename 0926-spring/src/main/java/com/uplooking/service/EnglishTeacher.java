package com.uplooking.service;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service("et")
public class EnglishTeacher implements Teacher{

	@Override
	public void teach() {
		
		System.out.println("英语老师教数学");
		
	}

}
