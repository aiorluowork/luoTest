package com.uplooking.pojo;

import java.util.HashMap;
import java.util.Map;

public class Pilot {
    
	private String name;
	private int exp;
	private Map<String,String> ids;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getExp() {
		return exp;
	}
	public void setExp(int exp) {
		this.exp = exp;
	}
	public Map<String, String> getIds() {
		return ids;
	}
	public void setIds(Map<String, String> ids) {
		this.ids = ids;
	}
	
	
}
