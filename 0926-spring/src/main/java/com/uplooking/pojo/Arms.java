package com.uplooking.pojo;

public class Arms {
    
	private String name;
	private int power;
	
	public Arms() {
		System.out.println("-----构造方法");
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPower() {
		return power;
	}
	public void setPower(int power) {
		this.power = power;
	}
	
	public void init() {
		System.out.println("初始化方法");
	}
	
	public void destroy() {
		System.out.println("摧毁方法");
	}
	
	
	
}
