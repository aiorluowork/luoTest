package com.uplooking.pojo;

import java.util.List;

public class Fighter {
    
	private String name;
	private int age;
	private List<Arms> arms;
	
	private Pilot pilot;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public List<Arms> getArms() {
		return arms;
	}

	public void setArms(List<Arms> arms) {
		this.arms = arms;
	}

	public Pilot getPilot() {
		return pilot;
	}

	public void setPilot(Pilot pilot) {
		this.pilot = pilot;
	}
	
	
}
