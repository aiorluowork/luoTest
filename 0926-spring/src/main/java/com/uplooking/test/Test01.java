package com.uplooking.test;

import java.util.List;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.uplooking.pojo.Arms;
import com.uplooking.pojo.Fighter;
import com.uplooking.pojo.Pilot;
import com.uplooking.pojo.User;
import com.uplooking.service.Clazz;

public class Test01 {
     
	
	public void test01() {
		//获取spring容器 
		ApplicationContext applicationContext=new 
				ClassPathXmlApplicationContext("applicationContext.xml");
		
		System.out.println("----------------------");
		
		//获取容器里面的元素
		Arms a1=(Arms) applicationContext.getBean("a1");
		
		Arms a2=(Arms) applicationContext.getBean("a1");
		
		System.out.println(a1==a2);
		
		//System.out.println(a1.getName()+"   "+a1.getPower());
		
		((AbstractApplicationContext) applicationContext).close();
		
	}
	
	
	public void test02() {
		//获取spring容器 
				ApplicationContext applicationContext=new 
						ClassPathXmlApplicationContext("applicationContext.xml");
				
				Pilot p=(Pilot) applicationContext.getBean("p1");
				System.out.println(p.getName()+"   "+p.getIds());
				
	}
	
	
	public void test03() {
		//获取spring容器 
				ApplicationContext applicationContext=new 
						ClassPathXmlApplicationContext("applicationContext.xml");
				
				Fighter j20=(Fighter) applicationContext.getBean("j20");
				
				System.out.println(j20.getName()+"   "+j20.getPilot().getIds());
				
				List<Arms> arms=j20.getArms();
				for (Arms arms2 : arms) {
					System.out.println(arms2.getName()+"   "+arms2.getPower());
				}
				
	}
	
	
    @Test
	public void test04() {
		//获取spring容器 
				ApplicationContext applicationContext=new 
						ClassPathXmlApplicationContext("applicationContext.xml");
				
				User user=(User) applicationContext.getBean("u1");
				User user2=(User) applicationContext.getBean("u1");
				user.setName("小黑");
				
				System.out.println(user2==user);
				
	}
	
    
    
    @Test
  	public void test05() {
  		//获取spring容器 
  		ApplicationContext applicationContext=new 
  						ClassPathXmlApplicationContext("applicationContext.xml");
  				
  				
  		Clazz c=applicationContext.getBean(Clazz.class);
  		
  		c.clazz();
  	}
  	
	
	
	
	
	
	
	
	
	
} 
