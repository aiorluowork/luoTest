package com.uplooking.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.uplooking.pojo.User;

public interface UserMapper {
	
	int add(User user);
	User findById(int uid);
	
	List<User> findByPwdAndAge(@Param("pwd")String pwd, @Param("age")int age);
	
	List<User> findByIds(@Param("uids") List<Integer> uids);
	
	int updateById(@Param("user") User user);
	
	int deleteBySome(@Param("uids") List<Integer> uids);
}
