package com.uplooking.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import com.uplooking.mapper.UserMapper;
import com.uplooking.pojo.User;

public class Test01 {
	SqlSession sqlSession;
	@Before
	public void test01() {
		
		SqlSessionFactory sessionFactory;
		try {
			// 建立会话工厂 通过工厂构造器，以流的形式加载配置文件
			sessionFactory = new SqlSessionFactoryBuilder().build(Resources.getResourceAsStream("SqlMapConfig.xml"));

			// 开启会话
			sqlSession = sessionFactory.openSession();

			// User user = sqlSession.selectOne("findById", 63);
			// System.out.println(user.getUid()+" "+user.getName());

			// List<User> users = sqlSession.selectList("checkall");
			// for (User user2 : users) {
			// System.out.println(user2.getUid()+" "+user2.getName());
			// }

			

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	
	public void test02() {
		
		UserMapper userMapper=sqlSession.getMapper(UserMapper.class);
		User user = userMapper.findById(1);
		System.out.println(user.getUid()+"   "+user.getName());
	}
	
	
	public void test03() {
		UserMapper userMapper=sqlSession.getMapper(UserMapper.class);
		List<User> users = userMapper.findByPwdAndAge("123",5);
		System.out.println(users.size());
		
	}
	
	
	public void test04() {
		List<Integer> uids = new ArrayList<Integer>();
		uids.add(1);
		uids.add(66);
		uids.add(68);
		
		UserMapper userMapper=sqlSession.getMapper(UserMapper.class);
		List<User> users = userMapper.findByIds(uids);
		
		System.out.println(users.size());
	}
	
	
	public void test05() {
		
		UserMapper userMapper=sqlSession.getMapper(UserMapper.class);
		User user = userMapper.findById(1);
		
		System.out.println(user.getUid());
		user.setName("lmz");
		
		int result = userMapper.updateById(user);
		sqlSession.commit();
		System.out.println(result>0?"更新成功":"更新失败");
	}
	
	@Test
	public void test06() {
		List<Integer> uids = new ArrayList<Integer>();
		uids.add(888);
		uids.add(889);
		uids.add(909); 
		
		UserMapper userMapper=sqlSession.getMapper(UserMapper.class);
		int result = userMapper.deleteBySome(uids);
		sqlSession.commit();
		System.out.println(result>0?"更新成功":"更新失败");
	}

}
